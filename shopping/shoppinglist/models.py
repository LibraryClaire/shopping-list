from django.db import models

# Create your models here.

class Item(models.Model):
    name = models.CharField(max_length=200) 
    complete = models.BooleanField(default=False) 
    unavailable = models.BooleanField(default=False)


    def __str__(self):
        return self.name

class Tag(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Tag_Assignment(models.Model):
    item = models.ForeignKey(to="Item", on_delete=models.CASCADE)
    tag = models.ForeignKey(to="Tag", on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return str(self.item)

