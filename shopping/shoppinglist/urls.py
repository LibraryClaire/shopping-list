"""shopping URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.shopping, name='shopping'),
    path('complete', views.complete, name='complete'),
    path('unavailable', views.unavailable, name='unavailable'),
    path('new', views.new, name='new'),
    path('item-complete/<int:item_id>', views.item_complete, name='item_complete'),
    path('item-unavailable/<int:item_id>', views.item_unavailable, name='item_unavailable'),
    path('complete-open/<int:item_id>', views.complete_open, name='complete_open'),
    path('unavailable-open/<int:item_id>', views.unavailable_open, name='unavailable_open'),
    path('delete-alldone', views.delete_alldone, name='delete_alldone'),
    path('all-available', views.all_available, name='all_available'),
    path('loeschen/<int:item_id>', views.loeschen, name='loeschen'),
    path('loeschen-c/<int:item_id>', views.loeschen_c, name='loeschen_c'),
    path('loeschen-u/<int:item_id>', views.loeschen_u, name='loeschen_u'),
    path('tag', views.tag, name='tag'),
    path('tag-assignment', views.tag_assignment, name='tag_assignment'),

]
