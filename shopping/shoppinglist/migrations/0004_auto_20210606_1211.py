# Generated by Django 3.2 on 2021-06-06 12:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoppinglist', '0003_rename_shop_item'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
        ),
        migrations.RemoveField(
            model_name='item',
            name='tagname',
        ),
        migrations.CreateModel(
            name='Tag_Assignment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shoppinglist.item')),
                ('tag', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='shoppinglist.tag')),
            ],
        ),
    ]
