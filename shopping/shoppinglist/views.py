from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from .forms import AddItemTag

# Create your views here.

from . import models

def shopping(request):
    context ={}
    context["shoppinglist"] = models.Item.objects.filter(complete=False) & models.Item.objects.filter(unavailable=False)
    #context["shoppinglist"] = models.Item.objects.filter(unavailable=False)
    return render(request, "shopping/shoppinglist.html", context)

def complete(request):
    context = {}
    context["shoppinglist"] = models.Item.objects.filter(complete=True)
    return render(request, "shopping/complete.html", context)

def unavailable(request):
    context ={}
    context["shoppinglist"] = models.Item.objects.filter(unavailable=True)
    return render(request, "shopping/unavailable.html", context)

def new(request):
    if request.POST:
        new_item = models.Item()
        new_item.name = request.POST["name"]
        new_item.save()
        new_tag = models.Tag()
        new_tag.name = request.POST["tagname"]
        new_tag.save()
        new_tag_assignment = models.Tag_Assignment()
        new_tag_assignment.item = new_item
        new_tag_assignment.tag = new_tag
        new_tag_assignment.save()
    return redirect("shopping")

def tag_assignment(request):
    for ta in item.tag_assignment_set.all():
        ta.tag.name
    return redirect("shopping")

def tag(request):
    if request.POST:
        new_tag = models.Tag()
        new_tag.name = request.POST["name"]
        new_tag.save()
    return redirect ("shopping")

def item_complete(request, item_id):
    item = get_object_or_404(models.Item, pk=item_id)
    item.complete = True
    item.save()
    return redirect("shopping")

def item_unavailable(request, item_id):
    item = get_object_or_404(models.Item, pk=item_id)
    item.unavailable = True
    item.save()
    return redirect("shopping")

def complete_open(request, item_id):
    item = get_object_or_404(models.Item, pk=item_id)
    item.complete = False
    item.save()
    return redirect("complete")

def unavailable_open(request, item_id):
    item = get_object_or_404(models.Item, pk=item_id)
    item.unavailable = False
    item.save()
    return redirect("unavailable")

def delete_alldone(request):
    models.Item.objects.filter(complete=True).delete()
    return redirect("shopping")

def all_available(request):
    models.Item.objects.filter(unavailable=True).update(unavailable=False)
    return redirect("shopping")

def loeschen(request, item_id):
    item = get_object_or_404(models.Item, pk=item_id)
    item.delete()
    return redirect("shopping")

def loeschen_c(request, item_id):
    item = get_object_or_404(models.Item, pk=item_id)
    item.delete()
    return redirect("complete")

def loeschen_u(request, item_id):
    item = get_object_or_404(models.Item, pk=item_id)
    item.delete()
    return redirect("unavailable")