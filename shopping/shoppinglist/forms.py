from django import forms

class AddItemTag(forms.Form):
    item = forms.CharField(max_length=200)
    tag = forms.CharField(max_length=200)